﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using zbus;

namespace zbus.Examples
{ 
    class PubExample
    { 
        static async Task Test()
        {
            using (MqClient client = new MqClient("localhost:15555"))
            {
                //1) Create MQ if necessary(empty in zbus), you may ommit this step
                const string mq = "MyMQ";
                const string channel = "MyChannel";
                const string topic = "MyTopic";
                Message data = new Message();
                data.Headers["cmd"] = "create";
                data.Headers["mq"] = mq;
                //data.Headers["channel"] = channel;
                await client.ConnectAsync();
                var res = await client.InvokeAsync(data);
                Console.WriteLine(JsonKit.SerializeObject(res));

                int i = 0;
                while (i < 10000)
                {
                    //2) Publish Message
                    data = new Message();
                    data.Headers["cmd"] = "pub";
                    data.Headers["mq"] = mq;
                    //data.Headers["channel"] = channel;

                    //data.Headers["topic"] = topic;
                    data.Body = "Hello from C# " + DateTime.Now;

                    res = await client.InvokeAsync(data);

                    Console.WriteLine(JsonKit.SerializeObject(res));

                    Thread.Sleep(1000);
                }
            }
        }
        static void Main(string[] args)
        {
            Test().Wait();
            Console.ReadKey();
        } 
    }
}
