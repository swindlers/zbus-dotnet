﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using zbus;

namespace zbus.Examples
{
    public interface IRpcService
    {
        string echo(string msg);

        // direction: 1-buy 2-sell type: 1-limit 2-market
        string CreateOrder(string productCode, decimal amount, decimal price, int direction, int type = 1);

        string CancelOrder(string productCode, string orderId);

        void noReturn();

        int plus(int a, int b);

        void throwException();


        Task<string> getString(string req); //GetStringAsync => getString for inter-language purpose only

    }

    class ApiServerTestSub
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ApiServerTestSub));
        static void Main(string[] args)
        {
            logger.Info("123412341234");

            MqClient client = new MqClient("localhost:80");

            client.AuthEnabled = true;
            client.ApiKey = "2ba912a8-4a8d-49d2-1a22-198fd285cb06";
            client.SecretKey = "461277322-943d-4b2f-b9b6-3f860d746ffd";

            const string depthMqHuobi = "QD_HUOBI_DEPTH";
            const string depthMqBinance = "QD_BINANCE_DEPTH";
            const string depthMqOKEx = "QD_OKEX_DEPTH";
            const string depthMqBFX = "QD_BITFINEX_DEPTH";
            const string balanceMq = "QD_HUOBI_BALANCE";
            const string filter = "BTC_USDT";
            const string filterBFX = "BTC_USD";
            client.OnOpen += async (c) =>
            {
                Message data = new Message();
                Message res = null;

                data.Headers["cmd"] = "create";
                data.Headers["mq"] = depthMqBinance;
                data.Headers["channel"] = depthMqBinance + "_MyChannel";
                res = await client.InvokeAsync(data);
                Console.WriteLine(JsonKit.SerializeObject(res));

                data = new Message();
                data.Headers["cmd"] = "sub";
                data.Headers["mq"] = depthMqBinance;
                data.Headers["channel"] = depthMqBinance + "_MyChannel";
                data.Headers["filter"] = filter;

                res = await client.InvokeAsync(data);
                Console.WriteLine(JsonKit.SerializeObject(res));

                data = new Message();
                data.Headers["cmd"] = "create";
                data.Headers["mq"] = balanceMq;
                data.Headers["channel"] = balanceMq + "_MyChannel";
                res = await client.InvokeAsync(data);
                Console.WriteLine(JsonKit.SerializeObject(res));

                data = new Message();
                data.Headers["cmd"] = "sub";
                data.Headers["mq"] = balanceMq;
                data.Headers["channel"] = balanceMq + "_MyChannel";
                //data.Headers["filter"] = filter;

                res = await client.InvokeAsync(data);
                Console.WriteLine(JsonKit.SerializeObject(res));
            };

            client.AddMqHandler(depthMqBinance, depthMqBinance + "_MyChannel", (msg) =>
            {
                Console.WriteLine("received " + DateTime.Now + JsonKit.SerializeObject(msg).ToString().Substring(0, 150));
            });
            client.AddMqHandler(balanceMq, balanceMq + "_MyChannel", (msg) =>
            {
                Console.WriteLine("received " + DateTime.Now + JsonKit.SerializeObject(msg).ToString().Substring(0, 150));
            });

            client.ConnectAsync().Wait();

            //using (RpcClient rpc = new RpcClient("localhost:15555", "QD_RPC"))
            //{
            //    //Raw API
            //    string module = "";
            //    //dynamic
            //    IRpcService svc = rpc.CreateProxy<IRpcService>(module);

            //    //svc.CreateOrder("ETH_USDT-HUOBI", 0.01m, 600, 2);
            //    string ret = svc.CancelOrder("ETH_USDT-HUOBI", "6594944729");

            //    //string str = await svc.getString("hong"); //support remote await!
            //    //Console.WriteLine(str);

            //    //int res = await rpc.InvokeAsync<int>("plus", new object[] { 1, 2 }, module: module);
            //    //Console.WriteLine(JsonKit.SerializeObject(res));
            //}
        }
    }
}
