﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using zbus;

namespace zbus.Examples
{
    class ApiServerTestRpcClient
    {
        static void Test()
        {
            using (RpcClient rpc = new RpcClient("192.168.1.156:15555"))
            {
                //rpc.AuthEnabled = true;
                //rpc.ApiKey = "2ba912a8-4a8d-49d2-1a22-198fd285cb06";
                //rpc.SecretKey = "461277322-943d-4b2f-b9b6-3f860d746ffd";

                //dynamic
                IService svc = rpc.CreateProxy<IService>("/rpcmq/qdapi");
                //string orderId = "123456";
                //var orderId2 = svc.QueryOrder(orderId);
                string productCode = "ETH_BTC-OKEX";
                svc.Subscribe(productCode);
                Console.WriteLine(productCode);
            }
        }
        static void Main(string[] args)
        {
            try
            {
                Test();
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }
                Console.WriteLine(e);
            }

            Console.ReadKey();
        }
    }
}
